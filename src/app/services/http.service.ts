import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IUser } from '../components/common/type';

@Injectable({
  providedIn: 'root',
})
export class HttpService {
  baseUrl: string = 'https://1c1f-103-36-44-106.in.ngrok.io';
  constructor(private http: HttpClient) {}

  postUsers(bodyData: IUser, subdirectory: string) {
    // console.log(bodyData);
    // console.log(`${this.baseUrl}/${subdirectory}`, bodyData);
    return this.http.post(`${this.baseUrl}/${subdirectory}`, bodyData);
  }

  getUsers(token: string, id: number) {
    console.log(token);
    console.log(id);
    return this.http.get(`${this.baseUrl}/user/getUser/${id}`, {
      headers: {
        Authorization: `Bearer ${token}`,
        'ngrok-skip-browser-warning': '1234',
      },
    });
  }
}
