import { Component, OnInit, Input } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { IUser } from '../common/type';

@Component({
  selector: 'app-display-details',
  templateUrl: './display-details.component.html',
  styleUrls: ['./display-details.component.scss'],
})
export class DisplayDetailsComponent implements OnInit {
  @Input('token') token: string = '';
  @Input('id') id: number = 0;
  userDetails!: IUser;

  displayDetails: boolean = false;
  displayError: boolean = false;
  errorMessage: string = '';
  constructor(private httpData: HttpService) {}

  ngOnInit(): void {}

  getDetails() {
    this.httpData.getUsers(this.token, this.id).subscribe({
      next: (response: IUser) => {
        console.log(response);
        this.displayError = false;
        this.userDetails = response;
        this.displayDetails = !this.displayDetails;
      },
      error: (error) => {
        if (error.status === 403) {
          this.errorMessage = 'Login first to access the Details!';
          this.displayError = !this.displayError;
        } else {
          this.errorMessage = 'Please Login before accessing the Data';
          this.displayError = true;
        }
      },
    });
  }

  onLogOut() {
    window.location.reload();
  }
}
