export interface IUser {
  userId?: number;
  userName?: string;
  userEmail?: string;
  password?: string;
  userCity?: string;
  userRole?: string;
  userSalary?: string;
}

export interface Itoken {
  jwtToken: string;
  jwtId: number;
}
