import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { IUser } from '../common/type';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
})
export class SignupComponent {
  displayMessage: boolean = false;
  displayError: boolean = false;
  username: string = '';
  email: string = '';
  password: string = '';
  userCity: string = '';
  userRole: string = '';
  userSalary: string = '';
  constructor(private httpData: HttpService) {}

  onSubmitDetails() {
    this.httpData
      .postUsers(
        {
          userName: this.username,
          userEmail: this.email,
          password: this.password,
          userCity: this.userCity,
          userRole: this.userRole,
          userSalary: this.userSalary,
        },
        'register'
      )
      .subscribe({
        next: (response: IUser) => {
          this.displayMessage = true;
          console.log(response);
        },
        error: (error) => {
          console.log(error);
          this.displayError = true;
        },
      });
  }
}
