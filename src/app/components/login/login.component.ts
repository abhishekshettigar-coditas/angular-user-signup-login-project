import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from 'src/app/services/http.service';
import { Itoken, IUser } from '../common/type';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  displayMessage: boolean = false;
  message: string = '';
  loginUsername: string = '';
  loginPassword: string = '';

  @Output('sendToken') sendToken = new EventEmitter();
  constructor(private httpData: HttpService) {}

  ngOnInit(): void {}
  onlogin() {
    this.httpData
      .postUsers(
        { userEmail: this.loginUsername, password: this.loginPassword },
        'login'
      )
      .subscribe({
        next: (response: any) => {
          // console.log(response);
          // console.log(response.jwtToken);
          // console.log(response.jwtId);
          this.displayMessage = true;
          this.message = 'You have logged In successfully!';
          this.loginUsername = '';
          this.loginPassword = '';

          this.sendToken.emit({ token: response.jwtToken, id: response.jwtId });
        },
        error: (error) => {
          console.log(error.status);
          this.displayMessage = true;
          this.message = 'Invalid Credentials';
        },
      });
  }
}
