import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  token: string = '';
  id!: number;
  title = 'signup-login';
  getDetails(token: any) {
    this.token = token.token;
    this.id = token.id;
  }
}
